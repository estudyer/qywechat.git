<?php

/**
 * 企业微信小程序客户端总调度器
 * 
 *
 */

namespace Estudyer\Qywx\Wx;

class Client
{

    private \Estudyer\Qywx\Client $_client;

    /**
     * @param \Estudyer\Qywx\Client $client
     */
    public function __construct(\Estudyer\Qywx\Client $client)
    {
        $this->_client = $client;
    }
}
