<?php

/**
 * 发票控制器
 *
 */

namespace Estudyer\Qywx\Manager\Card;

use Estudyer\Qywx\Client;
use Estudyer\Qywx\Manager\Card\Invoice\Reimburse;

class Invoice
{
    private $_client;

    private $_request;

    public function __construct(Client $client)
    {
        $this->_client = $client;
        $this->_request = $client->getRequest();
    }

    /**
     * 获取电子发票对象
     *
     * @return \Estudyer\Qywx\Manager\Card\Invoice\Reimburse
     */
    public function getReimburseManager()
    {
        return new Reimburse($this->_client);
    }
}
