<?php

/**
 * 卡控制器
 *
 */

namespace Estudyer\Qywx\Manager;

use Estudyer\Qywx\Client;
use Estudyer\Qywx\Manager\Card\Invoice;

class Card
{

    private $_client;

    private $_request;

    public function __construct(Client $client)
    {
        $this->_client = $client;
        $this->_request = $client->getRequest();
    }

    /**
     * 获取发票对象
     *
     * @return \Estudyer\Qywx\Manager\Card\Invoice
     */
    public function getInvoiceManager()
    {
        return new Invoice($this->_client);
    }
}
